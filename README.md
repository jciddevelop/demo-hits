## Instalación y ejecución de Proyecto Backend
A continuación les comento lo necesario para levantar este proyecto requerido para la prueba BACKEND del TEST:
Tener instalado MAVEN.
Una vez descargado el repositorio deben ejecutar en el directorio del proyecto el siguiente comando

### mvn clean install
Esta instrucción será la encargada de descargar los paquetes de los plugins y módulos utilizados y necesarios para la ejecución.
El archivo pom.xml contiene las referencias de todo lo anterior, y es la referencia que necesita maven para instalarlos.

## Base de datos
Según lo requerido se utilizo una base de datos MongoDB, la que se encuentra alojada en los servidores de mLab https://mlab.com/.
Las credenciales y string connection están indicadas en el archivo application.properties del proyecto y en caso de 
requerir cambiarlas deben modificar este archivo

## Inicialización de DB
Actualmente la base de datos está poblada, en caso que se requiera comenzar en otra base de datos o inicializar la actual,
deberán borrar las colecciones de la tabla HIT, y luego correr el script indicado en el archivo init.js agregado a este 
proyecto en la siguiente ruta:

### demo-hits/src/main/resources/static/init.js


## Una vez instalado ejecutar el proyecto con la siguiente instrucción:

### mvn spring-boot:run
Este comando dejara inicializada la api en puerto 8080

