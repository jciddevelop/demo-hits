package com.example.demohits.service;

import com.example.demohits.component.CronProcess;
import com.example.demohits.entity.Hit;
import com.example.demohits.repository.HitRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service("hitServiceImpl")
public class HitServiceImpl implements HitService{

    private static final Log Log = LogFactory.getLog(HitServiceImpl.class);

    @Autowired
    @Qualifier("hitRepository")
    private HitRepository hitRepository;

    @Override
    public List<Hit> getAllHits() {
        return hitRepository.findAll().stream().filter(x -> x.getDeleted() == 0).collect(Collectors.toList());
    }

    @Override
    public Hit getHitByObjectId(String id) {
        return hitRepository.findById(id).get();
    }

    @Override
    public Hit addHit(Hit hit) {
        return hitRepository.save(hit);
    }

    @Override
    public List<Hit> addHitsinBatch(List<Hit> hits) {
        return hitRepository.saveAll(hits);
    }

    @Override
    public Hit deleteHit(String id) {
        Hit found = hitRepository.findByObjectID(id);
        ObjectId objectId = new ObjectId(found.get_id());
        found.setDeleted(1);
        return hitRepository.save(found);
    }

    @Override
    public boolean hitsFillProccess()  {
        ArrayList<Hit> hits = new ArrayList<Hit>();
        JsonNode json = null;
        try {
            json = handleRequest();
        } catch (Exception e) {
            System.out.println("ERROR EN PROCESO hitsFillProccess::: ".concat(e.getMessage()));
            return false;
        }
        for(JsonNode x : json) {
            Hit hit  = parseObject(x);
            validateHitDeleted(hit);
        }

        return true;
    }

    private JsonNode handleRequest() throws IOException {
        RestTemplate rt = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        String url = "https://hn.algolia.com/api/v1/search_by_date?query=nodejs";
        ResponseEntity<String> res = rt.exchange(url, HttpMethod.GET, entity, String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(res.getBody());
        JsonNode data = root.path("hits");

        return data;
    }

    private Hit parseObject(JsonNode json) {
        Hit hit = new Hit();

        String created_at = "";
        String title = "";
        String url = "";
        String author = "";
        float points = 0;
        String story_text = "";
        String comment_text = "";
        float num_comments = 0;
        float story_id = 0;
        String story_title = "";
        String story_url = "";
        float parent_id = 0;
        float created_at_i = 0;
        float relevancy_score = 0;
        ArrayList< Object > _tags = new ArrayList < Object > ();
        String objectID = "";
        String _highlightResult = "";

        try {
            created_at = json.path("created_at").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("created_at ::: ");
        }
        try {
            title = json.path("title").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("title ::: ");
        }
        try {
            url = json.path("url").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("url ::: ");
        }
        try {
            author = json.path("author").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("author ::: ");
        }
        try {
            points = json.path("points").floatValue();
        } catch (Exception  e) {
            Log.info("points ::: ");
        }
        try {
            story_text = json.path("story_text").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("story_text ::: ");
        }
        try {
            comment_text = json.path("comment_text").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("comment_text ::: ");
        }
        try {
            num_comments = json.path("num_comments").floatValue();
        } catch (Exception  e) {
            Log.info("num_comments ::: ");
        }
        try {
            story_id = json.path("story_id").floatValue();
        } catch (Exception  e) {
            Log.info("story_id ::: ");
        }
        try {
            objectID = json.path("objectID").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("objectID ::: ");
        }
        try {
            story_title = json.path("story_title").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("story_title ::: ");
        }
        try {
            story_url = json.path("story_url").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("story_url ::: ");
        }
        try {
            parent_id = json.path("parent_id").floatValue();
        } catch (Exception  e) {
            Log.info("parent_id ::: ");
        }
        try {
            created_at_i = json.path("created_at_i").floatValue();
        } catch (Exception  e) {
            Log.info("created_at_i ::: ");
        }
        try {
            relevancy_score = json.path("relevancy_score").floatValue();
        } catch (Exception  e) {
            Log.info("relevancy_score ::: ");
        }
        try {
            String tagObject = json.path("_tags").toString().replace("\"", "");;
            _tags.add(tagObject);
        } catch (Exception  e) {
            Log.info("_tags ::: ");
        }
        try {
            _highlightResult = json.path("_highlightResult").get("title").path("value").toString().replace("\"", "");;
        } catch (Exception  e) {
            Log.info("_highlightResult ::: ");
        }

        hit.setObjectID(objectID);
        hit.setCreated_at(created_at);
        hit.setTitle(title);
        hit.setUrl(url);
        hit.setAuthor(author);
        hit.setPoints(points);
        hit. setStory_text(story_text);
        hit.setComment_text(comment_text);
        hit.setNum_comments(num_comments);
        hit.setStory_id(story_id);
        hit.setStory_title(story_title);
        hit.setStory_url(story_url);
        hit.setParent_id(parent_id);
        hit.setCreated_at_i(created_at_i);
        hit.setRelevancy_score(relevancy_score);
        hit.set_tags(_tags);
        hit.set_highlightResult(_highlightResult);

        return hit;
    }

    private boolean validateHitDeleted(Hit hit) {
        Hit found = hitRepository.findByObjectID(hit.getObjectID());

        if(found == null) {
            hit.setDeleted(0);
            hitRepository.save(hit);
        }
        else {
            ObjectId objectId = new ObjectId(found.get_id());
            hit.set_id(objectId);
            if(found.getDeleted() == 0) {
                hitRepository.save(hit);
            }
        }


        return true;
    }

}
