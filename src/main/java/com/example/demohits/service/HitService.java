package com.example.demohits.service;

import com.example.demohits.entity.Hit;
import java.io.IOException;
import java.util.List;

public interface HitService  {
    public abstract List<Hit> getAllHits();
    public abstract Hit getHitByObjectId(String id);
    public abstract Hit addHit(Hit hit);
    public abstract List<Hit> addHitsinBatch(List<Hit> hits);
    public abstract Hit deleteHit(String id);
    public abstract boolean hitsFillProccess() throws IOException;
 }
