package com.example.demohits.repository;

import com.example.demohits.entity.Hit;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


public interface HitRepository extends MongoRepository<Hit, String> {

    Hit findByObjectID(String id);
}
