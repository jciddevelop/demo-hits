package com.example.demohits.component;

import com.example.demohits.service.HitServiceImpl;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CronProcess {

    private static final Log Log = LogFactory.getLog(CronProcess.class);

    @Autowired
    @Qualifier("hitServiceImpl")
    private HitServiceImpl hitService;

    @Scheduled(fixedRate = 3600000)
    public void process() throws IOException {
        Log.info("COMIENZA PROCESO DE LLENADO DE TABLA HIT::: ");
        hitService.hitsFillProccess();
    }
}
