package com.example.demohits.controller;

import com.example.demohits.entity.Hit;
import com.example.demohits.service.HitServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.DELETE})
@RequestMapping("/api/v1/hits")
public class HitController {

    @Autowired
    @Qualifier("hitServiceImpl")
    private HitServiceImpl hitService;


    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Hit> getAllPets() {
        return hitService.getAllHits();
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Hit createPet(@RequestBody Hit hit) {
        return hitService.addHit(hit);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePet(@PathVariable("id") String id) {
        hitService.deleteHit(id);
    }

}
