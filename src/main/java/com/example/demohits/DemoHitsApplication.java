package com.example.demohits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DemoHitsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoHitsApplication.class, args);

	}



}
