package com.example.demohits.entity;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Hit {
    @Id
    public ObjectId _id;

    private String created_at;
    private String title;
    private String url;
    private String author;
    private float points;
    private String story_text;
    private String comment_text;
    private float num_comments;
    private float story_id;
    private String story_title;
    private String story_url;
    private float parent_id;
    private float created_at_i;
    private float relevancy_score;
    private ArrayList< Object > _tags = new ArrayList < Object > ();
    private String objectID;
    private String _highlightResult;
    private float deleted;



    public Hit() {
    }

    public Hit(ObjectId _id, String created_at, String title, String url, String author, float points, String story_text, String comment_text, float num_comments, float story_id, String story_title, String story_url, float parent_id, float created_at_i, float relevancy_score, ArrayList<Object> _tags, String objectID, String _highlightResult, float deleted) {
        this._id = _id;
        this.created_at = created_at;
        this.title = title;
        this.url = url;
        this.author = author;
        this.points = points;
        this.story_text = story_text;
        this.comment_text = comment_text;
        this.num_comments = num_comments;
        this.story_id = story_id;
        this.story_title = story_title;
        this.story_url = story_url;
        this.parent_id = parent_id;
        this.created_at_i = created_at_i;
        this.relevancy_score = relevancy_score;
        this._tags = _tags;
        this.objectID = objectID;
        this._highlightResult = _highlightResult;
        this.deleted = deleted;
    }

    public String get_id() { return _id.toHexString(); }
    public void set_id(ObjectId _id) { this._id = _id; }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public String getStory_text() {
        return story_text;
    }

    public void setStory_text(String story_text) {
        this.story_text = story_text;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public float getNum_comments() {
        return num_comments;
    }

    public void setNum_comments(float num_comments) {
        this.num_comments = num_comments;
    }

    public float getStory_id() {
        return story_id;
    }

    public void setStory_id(float story_id) {
        this.story_id = story_id;
    }

    public String getStory_title() {
        return story_title;
    }

    public void setStory_title(String story_title) {
        this.story_title = story_title;
    }

    public String getStory_url() {
        return story_url;
    }

    public void setStory_url(String story_url) {
        this.story_url = story_url;
    }

    public float getParent_id() {
        return parent_id;
    }

    public void setParent_id(float parent_id) {
        this.parent_id = parent_id;
    }

    public float getCreated_at_i() {
        return created_at_i;
    }

    public void setCreated_at_i(float created_at_i) {
        this.created_at_i = created_at_i;
    }

    public float getRelevancy_score() {
        return relevancy_score;
    }

    public void setRelevancy_score(float relevancy_score) {
        this.relevancy_score = relevancy_score;
    }

    public ArrayList<Object> get_tags() {
        return _tags;
    }

    public void set_tags(ArrayList<Object> _tags) {
        this._tags = _tags;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public String get_highlightResult() {
        return _highlightResult;
    }

    public void set_highlightResult(String _highlightResult) {
        this._highlightResult = _highlightResult;
    }

    public float getDeleted() {
        return deleted;
    }

    public void setDeleted(float deleted) {
        this.deleted = deleted;
    }
}
