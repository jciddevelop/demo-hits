db.createCollection("hit");

db.hit.find({});


db.hit.insertMany(
    [
       {
          "created_at": "2019-03-30T17:55:28.000Z",
          "title": "",
          "deleted": 0,
          "url": "",
          "author": "tombert",
          "points": 1,
          "story_text": "",
          "comment_text": "I haven&#x27;t written anything in TypeScript, though I have some friends that love the language; I got annoyed at having to supply bindings for obscure languages.<p>If you can get past the initial hurdle, is TypeScript worth it for Node.JS stuff?",
          "num_comments": 2,
          "story_id": 19529308,
          "story_title": "TypeScript 3.4",
          "story_url": "https://devblogs.microsoft.com/typescript/announcing-typescript-3-4/",
          "parent_id": 19529308,
          "created_at_i": 1553968528,
          "relevancy_score": 8734,
          "_tags": [
             "comment",
             "author_tombert",
             "story_19529308"
          ],
          "objectID": "19530757",
          "_highlightResult": ""
       }
]);